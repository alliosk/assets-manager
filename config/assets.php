<?php

return [
    /**
     * Zoznam assetov s ich dependenciami, napr:
     * 'jquery' => [
     *      'js' => ['vendor/cms/libs/jquery/jquery-3.3.1.min.js'],
     * ],
     * 'bootstrap' => [
     *      'js' => ['vendor/cms/libs/bootstrap/js/bootstrap.min.js'],
     *      'css' => ['vendor/cms/libs/bootstrap/css/bootstrap.min.css'],
     *      'depends' => ['jquery']
     * ],
     * 'fontawesome' => [
     *     'css' => [
     *         '//use.fontawesome.com/releases/v5.1.0/css/all.css' => [
     *             "integrity" => "sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt",
     *             "crossorigin" => "anonymous"
     *         ],
     *     ]
     * ],
     * alebo
     * 'jquery' => \Path\To\JQueryBundle::class
     * ...
     */
    'bundles' => [
        
    ],
    
];