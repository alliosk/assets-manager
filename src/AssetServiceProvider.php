<?php

namespace Allio\Assets;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Facades\Blade;

class AssetServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;
    
    /**
     * Bootstrap the service provider.
     *
     * @return void
     */
    public function boot()
    {
        $this->setupConfig($this->app);
        $this->registerBladeDirectives();
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerResolver();

        $this->registerDispatcher();

        $this->registerAsset();
        
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    protected function registerAsset()
    {
        $this->app->singleton('allio.assets', function (Application $app) {
            return new Factory($app->make('allio.assets.dispatcher'), $app->make('config'));
        });
        $this->app->alias('allio.assets', Factory::class);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    protected function registerDispatcher()
    {
        $this->app->singleton('allio.assets.dispatcher', function (Application $app) {
            return new Dispatcher(
                $app->make('files'),
                $app->make('html'),
                $app->make('allio.assets.resolver'),
                $app->publicPath()
            );
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    protected function registerResolver()
    {
        $this->app->singleton('allio.assets.resolver', function () {
            return new DependencyResolver();
        });
    }

    /**
     * Setup the config.
     *
     * @param $app
     *
     * @return void
     */
    protected function setupConfig($app)
    {
        $config = realpath(__DIR__.'/../config/assets.php');

        if ($app->runningInConsole()) {
            $this->publishes([
                $config => base_path('config/assets.php'),
            ]);
        }

        $this->mergeConfigFrom($config, 'assets');
    }
    
    /** zaregistruje blade directive */
    protected function registerBladeDirectives()
    {
        Blade::directive('asset_bundle', function ($bundle) {
            return "<?php app('allio.assets')->bundle($bundle); ?>";
        });
        
        Blade::directive('css', function ($body) {
            // alias, filename , dependencies
            $body = str_replace(' ', '', $body);
            $body = str_replace("'", '', $body);
            $css = explode(',', $body);
            return "<?php app('allio.assets')->registerBundleFromArray(['css'=>['$css[1]'], 'depends' => ['".($css[2]??'')."']], '$css[0]'); ?>";
        });
    
        Blade::directive('bodyJs', function ($body) {
            $body = str_replace(' ', '', $body);
            $body = str_replace("'", '', $body);
            $js = explode(',', $body);
            return "<?php app('allio.assets')->registerBundleFromArray(['js'=>['$js[1]'], 'depends' => ['".($js[2] ?? '')."']], '$js[0]'); ?>";
        });
    }
    
    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['allio.assets', 'allio.assets.dispatcher', 'allio.assets.resolver'];
    }
    
    /**
     * Merge the given configuration with the existing configuration.
     *
     * @param  string  $path
     * @param  string  $key
     * @return void
     */
    protected function mergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);

        $this->app['config']->set($key, $this->mergeBundleConfig(require $path, $config));
    }
    
    /**
     * Merges the bundles configs together and takes multi-dimensional arrays into account.
     *
     * @param  array  $original
     * @param  array  $merging
     * @return array
     */
    protected function mergeBundleConfig(array $original, array $merging)
    {
        $overrides = $merging['bundles'];
        $return = array_merge_recursive($original, $merging);
        foreach ($return['bundles'] as $key => $value) {
            if (! is_array($value)) {
                continue;
            }
            if (! isset($overrides[$key])) {
                continue;
            }
            $return['bundles'][$key] = $overrides[$key];
        }
        return $return;
    }
}
