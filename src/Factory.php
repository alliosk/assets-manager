<?php

namespace Allio\Assets;

use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\Arr;

class Factory
{
    /**
     * Asset Dispatcher instance.
     *
     * @var \Allio\Assets\Dispatcher
     */
    protected $dispatcher;
    
    /** 
     * Laravel application config
     * 
     * @var Config 
     */
    protected $config;

    /**
     * All of the instantiated asset containers.
     *
     * @var array
     */
    protected $containers = [];

    /**
     * pole spracovanych asset bundle
     */
    protected $_asset_bundle = [];
    
    /**
     * Construct a new environment.
     *
     * @param  \Allio\Assets\Dispatcher  $dispatcher
     * @param  \Illuminate\Contracts\Config\Repository  $config
     */
    public function __construct(Dispatcher $dispatcher, Config $config)
    {
        $this->dispatcher = $dispatcher;
        $this->config = $config;
    }

    /**
     * Get an asset container instance.
     *
     * @param  string  $container
     *
     * @return \Allio\Assets\AssetContainer
     */
    protected function container(string $container = 'default'): AssetContainer
    {
        if (! isset($this->containers[$container])) {
            $this->containers[$container] = new AssetContainer($container, $this->dispatcher, $this->config);
        }

        return $this->containers[$container];
    }

    /**
     * Get the links to all of the registered JavaScript assets.
     *
     * @return string
     */
    public function scripts() : string
    {
        return $this->container('body')->scripts();
    }
    
    /**
     * Get the links to all of the registered CSS assets.
     *
     * @return string
     */
    public function styles() : string
    {
        return $this->container()->styles();
    }
    
    /**
     * @param string $bundle - identifikator skupiny, 
     */
    public function bundle(string $bundle){
        if($this->isBundleIncluded($bundle)){
            return;
        }
        
        $config_data = $this->config->get("assets.bundles.$bundle", "");
        // if bundle from config is defined as an array: ['js' => [], 'css' => []]
        if(is_array($config_data)){
            $this->registerBundleFromArray($config_data, $bundle);
        }
        // if bundle from config is defined as class ie: JQuery::class
        elseif(is_string($config_data) && class_exists($config_data) && is_subclass_of($config_data, \Allio\Assets\BundleBase::class)){
            $this->registerBundleFromClass(resolve($config_data), $bundle);
        }
        elseif(class_exists($bundle) && is_subclass_of($bundle, \Allio\Assets\BundleBase::class)){
            $this->registerBundleFromClass(resolve($bundle), NULL);
        }
    }
    
    public function registerBundleFromClass(\Allio\Assets\BundleBase $asset, $bundle_name = NULL){
        if($bundle_name === NULL){
            $bundle_name = $asset->getKey();
        }
        $this->registerBundleData($bundle_name, $asset->js()->all(), $asset->css()->all(), $asset->depends()->all());
    }
    
    public function registerBundleFromArray(array $config_data, string $bundle_name){
        $js = Arr::get($config_data, "js", []);
        $css = Arr::get($config_data, "css", []);
        $depends = Arr::get($config_data, "depends", []);
        $this->registerBundleData($bundle_name, $js, $css, $depends);
    }
    
    /**
     * @param string $bundle_name
     * @param array $js
     * @param array $css
     * @param array $depends
     */
    protected function registerBundleData(string $bundle_name, array $js, array $css, array $depends){
        $this->_asset_bundle[$bundle_name] = true;
        $i = 1;
        foreach($js as $js_file => $js_options){
            if(is_int($js_file)){
                $js_file = $js_options;
                $js_options = [];
            }
            $this->container('body')->script($bundle_name.($i == count($js) ? '' : $i), $js_file, array_merge($depends, [$bundle_name.($i-1)]), $js_options);
            $i++;
        }
        $i = 1;
        foreach($css as $css_file => $css_options){
            if(is_int($css_file)){
                $css_file = $css_options;
                $css_options = [];
            }
            $this->container()->style($bundle_name.($i == count($css) ? '' : $i), $css_file, array_merge($depends, [$bundle_name.($i-1)]), $css_options);
            $i++;
        }
        
        foreach($depends as $depencency){
            $this->bundle($depencency);
        }
    }
    
    /**
     * je zadany bundle zahrnuty do vystupu?
     * @param string $bundle
     * @return bool
     */
    public function isBundleIncluded(string $bundle) : bool
    {
        return isset($this->_asset_bundle[$bundle]);
    }
    
}