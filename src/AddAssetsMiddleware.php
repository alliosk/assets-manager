<?php

namespace Allio\Assets;

use Illuminate\Http\{Request};
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Closure;

class AddAssetsMiddleware
{
    private $_assets;
    
    
    /** {@inheritdoc} */
    public function __construct(Factory $assets){
        $this->_assets = $assets;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return Response
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $content_type = $response->headers->get('Content-Type');
        if(!is_a($response, StreamedResponse::class) && !$response->isRedirect() && (strlen($content_type) == 0 || strpos($content_type, 'text/html') !== false)){
            $this->addAssets($request, $response);
        }
        
        return $response;
    }
    
    /**
     * @param Request $request
     * @param Response $response
     * @return void
     */
    public function addAssets(Request $request, Response $response) : void
    {
        $content = $response->getContent();
        
        $head = $this->_assets->styles();
        $body = $this->_assets->scripts();
            
        $response->setContent(str_replace(
            ["</head>", "</body>"], 
            ["$head</head>", "$body</body>"], 
            $content
        ));
    }
}