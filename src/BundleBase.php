<?php

namespace Allio\Assets;

use Illuminate\Contracts\Config\Repository as Config;
use Illuminate\Support\{Arr, Collection};

abstract class BundleBase
{
    /** 
     * Laravel application config
     * 
     * @var Config 
     */
    protected $config;
    
    /**
     * @var array list of bundle class names that this bundle depends on.
     *
     * For example:
     *
     * ```php
     * public $depends = [
     *    'namespace\path\Asset',
     *    'namespace\bootstrap\BootstrapAsset',
     * ];
     * ```
     */
    protected $depends = [];
    
    /**
     * @var array list of JavaScript files that this bundle contains. 
     * 
     * Each JavaScript file can be specified in one of the following formats:
     * - an absolute URL representing an external asset. For example,
     *   `http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js` or
     *   `//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js`.
     * - a relative path representing a local asset (e.g. `js/main.js`) in public path
     * - an array where key is string representing path (see notes above) and value is array of js options to be merged with [[js_options]]
     *
     * Note that only a forward slash "/" should be used as directory separator.
     */
    protected $js = [];
    
    /**
     * @var array list of additional key=>value JS script tag attributes
     */
    protected $js_options = [];
    
    /**
     * @var array list of CSS files that this bundle contains. 
     * 
     * Each CSS file can be specified in one of the three formats as explained in [[js]].
     *
     * Note that only a forward slash "/" should be used as directory separator.
     */
    protected $css = [];
    
    /**
     * @var array list of key=>value CSS link tag attributes
     */
    protected $css_options = [];
    
    /**
     * Create a new bundle instance.
     *
     * @param  Config  $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->init();
    }
    
    /**
     * Run assets initialization.
     * Override this method to add dynamic parameters
     * @void 
     */
    protected function init()
    {
        
    }
    
    /**
     * Return identifier from asset config
     * 
     * @return string
     */
    public function getKey() : string
    {
        $assets = Arr::wrap($this->config->get("assets.bundles", []));
        $key = array_search(static::class, $assets);
        return $key ?: static::class;
    }

    /**
     * Return result Collection of JS dependencies.
     * 
     * @return Collection
     */
    public function js() : Collection
    {
        $return = new Collection([]);
        foreach($this->js as $js_file => $js_options){
            if(is_int($js_file)){
                $js_file = $js_options;
                $js_options = [];
            }
            $return[$js_file] = array_merge($this->js_options, $js_options);
        }
        return $return;
    }
    
    /**
     * Return result Collection of CSS dependencies.
     * 
     * @return Collection
     */
    public function css() : Collection
    {
        $return = new Collection([]);
        foreach($this->css as $css_file => $css_options){
            if(is_int($css_file)){
                $css_file = $css_options;
                $css_options = [];
            }
            $return[$css_file] = array_merge($this->css_options, $css_options);
        }
        return $return;
    }
    
    /**
     * Return result Collection of bundle dependencies.
     * 
     * @return Collection
     */
    public function depends() : Collection
    {
        return new Collection($this->depends);
    }
}
